var express = require('express')
  , router = express.Router()

router.use('/build', require('./build'))
router.use('/files', require('./files'))
router.use('/projects', require('./projects'))


module.exports = router
