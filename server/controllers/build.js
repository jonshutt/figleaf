var express = require('express');
var router = express.Router();
var PDFDocument = require('pdfkit')
var fs    = require('fs');
var SVGtoPDF    = require('svg-to-pdfkit');
var lodash    = require('lodash');
const resolve = require('path').resolve;

var fileExtRegex = /\.([0-9a-z]+)(?:[\?#]|$)/i;


PDFDocument.prototype.addSVG = function(svg, x, y, options) {
  var contents  = fs.readFileSync(svg, 'utf8');
  return SVGtoPDF(this, contents, x, y, options), this;
};


cmToPx = function(cm) {
  return (cm / 2.54) * 72
}

toLetters = function(num) {
    "use strict";
    var mod = num % 26,
        pow = num / 26 | 0,
        out = mod ? String.fromCharCode(64 + mod) : (--pow, 'Z');
    return pow ? toLetters(pow) + out : out;
}

router.post('/', function(req, res, next) {

  var pageWidth = cmToPx(req.body.project.width)
  var pageHeight = cmToPx(req.body.project.height)
  var pageMargin = cmToPx(req.body.project.margin || 1)
  var figSpacing = cmToPx(req.body.project.padding ||.5)

  var layout = req.body.project.layout

  var refNumberWidth = 15
  var refNumberHeight = 15

  layout.forEach((fig, key) => {
    fig.ref = toLetters(key+1)
  })

  doc = new PDFDocument({
    size: [pageWidth, pageHeight],
    margins : {
      top : 0,
      left: 0,
      right: 0,
      bottom: 0
    }
  })
  var filename = 'output.pdf'
  var path = `output/${filename}`

  doc.pipe(fs.createWriteStream(path))

  doc.font('Helvetica').fontSize(15)

  const rows = lodash.values(lodash.groupBy(lodash.sortBy(layout, 'row'), 'row'));

  const totalVerticalPadding = figSpacing * (rows.length-1)
  const totalVerticalFigSize = pageHeight - pageMargin - pageMargin - totalVerticalPadding
  const rowHeight = totalVerticalFigSize / rows.length

  const layoutFigures = [];

  rows.forEach((row, rowID) => {
    const totalHorizontalPadding = figSpacing * (row.length-1)
    const totalHorizontalFigSize = pageWidth - pageMargin - pageMargin - totalHorizontalPadding
    const colWidth = totalHorizontalFigSize / row.length

    let figY  = (rowHeight * rowID) + pageMargin + (figSpacing * rowID)


    row.forEach((fig, colID) => {
      let figX  = (colWidth * colID) + pageMargin + (figSpacing * colID)

      let addRight
      let addBelow
      const figHeight = rowHeight

      doc.fontSize(15).text(fig.ref, figX, figY)

      if (fig.label) {
        doc.fontSize(10).text(fig.label, figX, figY + rowHeight - 5, { width: colWidth, align: 'center'})
      }

      if (fig.url) {
        var ext = (fig.url).match(fileExtRegex)[1];
        if (ext === 'svg') {
          doc.addSVG(`files/${fig.url}`, figX+ refNumberWidth, figY +refNumberHeight, {width: colWidth- refNumberWidth, height: rowHeight - refNumberHeight - ((fig.label) ? 15 : 0)})
        }
        if (ext === 'png' || ext === 'jpg' || ext === 'jpeg') {
          doc.image(`files/${fig.url}`, figX+ refNumberWidth, figY +refNumberHeight, {
            fit: [colWidth- refNumberWidth, rowHeight - refNumberHeight - ((fig.label) ? 15 : 0)],
            align: 'center',
            valign: 'center'
          })
        }
      }

    })
  })

  doc.end()
  
  res.json({path: resolve(path), message: 'PDFDocument Created'});
});

module.exports = router;
