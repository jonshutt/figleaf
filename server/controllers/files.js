var express = require('express');
var router = express.Router();
var  fs = require('fs');

router.get('/', function(req, res, next) {

  fs.readdir('./files', (err, files) => {
    res.json({files});
  })
});

module.exports = router;
