var express = require('express');
var router = express.Router();
var fs = require('fs');

var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('database/db.sqlite');



router.get('/', function(req, res, next) {


  db.serialize(function() {
    db.run("CREATE TABLE IF NOT EXISTS projects (id INTEGER PRIMARY KEY AUTOINCREMENT)");
    db.all("PRAGMA table_info(projects)",  function(err, data) {

      if (data.filter((col) => col.name === 'name').length === 0) {
         db.run("alter table projects add column name TEXT");
      }
      if (data.filter((col) => col.name === 'width').length === 0) {
         db.run("alter table projects add column width INTEGER");
      }
      if (data.filter((col) => col.name === 'margin').length === 0) {
         db.run("alter table projects add column margin REAL");
      }
      if (data.filter((col) => col.name === 'padding').length === 0) {
         db.run("alter table projects add column padding REAL");
      }
      if (data.filter((col) => col.name === 'height').length === 0) {
         db.run("alter table projects add column height INTEGER");
      }
      if (data.filter((col) => col.name === 'layout').length === 0) {
         db.run("alter table projects add column layout blob");
      }
    });

    const projects = db.all("SELECT id, name, width, height, layout, margin, padding FROM projects", function(err, rows) {
        res.json({
          message: `${rows.length} Projects Opened`,
          projects: rows
        })
    });
  });

  // db.close();


});


router.post('/add', function(req, res, next) {
  db.serialize(function() {
    db.run("INSERT INTO projects (name, width, height, margin, padding) VALUES ('New', 21, 29.7, 1, .5)");
    res.json({
      message: 'Project Created'
    })
  });


});


router.post('/save', function(req, res, next) {
  var sql ="UPDATE projects SET name = ?, width = ?, height = ?, layout = ?, padding = ?, margin = ? WHERE id = ?"
  db.run(sql, [req.body.name, req.body.width, req.body.width, req.body.layout, req.body.padding, req.body.margin, req.body.id], function(err, rows) {
    res.json({
      message: 'Project Saved'
    })
  });
});

module.exports = router;
