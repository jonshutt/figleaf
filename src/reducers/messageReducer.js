import * as actionType from '../actions/ActionType';

const projectReducer = (state = '', action) => {
  switch (action.type) {
    case actionType.SET_MESSAGE:      
      return action.message
    default:
      return state
  }
}

export default projectReducer;
