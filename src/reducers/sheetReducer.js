import * as actionType from '../actions/ActionType';
import { maxBy  } from 'lodash';
import update from 'immutability-helper';


let nextID

const sheetReducer = (state = {
  width: 20,
  height: 15,
  figures: [
    {
      row: 1,
      id: 1
    }
  ]
}, action) => {
  switch (action.type) {
    case actionType.ADD_FIGURE:

      if (state.figures.length > 0) {
        nextID = maxBy(state.figures, 'id').id + 1;
      } else {
        nextID = 1;
      }

      const newFigure = {
        row: action.row,
        id: nextID
      }

      return {
        ...state,
        figures: [
          ...state.figures,
          newFigure
        ]
      }
    case actionType.SET_FIGURE_FILE:
      // console.log(action)

      return {
        ...state,
        figures: state.figures.map((fig) => fig.id === action.figureID ? { ...fig, url: action.url} : fig)
      }
      // return {
      //   ...state,
      //   figures: state.figures.map((fig) => {
      //     console.log(fig.id, action.figureID)
      //   })
      // }

    default:
      return state
  }
}

export default sheetReducer;
