import { combineReducers } from 'redux';

import filesReducer from './filesReducer';
import projectReducer from './projectReducer';
import selectedProjectReducer from './selectedProjectReducer';
import imageChooserReducer from './imageChooserReducer';
import messageReducer from './messageReducer';

const reducers = combineReducers({
  files: filesReducer,
  selectedProject: selectedProjectReducer,
  projects: projectReducer,
  imageChooser: imageChooserReducer,
  message: messageReducer,
})

export default reducers
