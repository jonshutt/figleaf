import * as actionType from '../actions/ActionType';
import { maxBy  } from 'lodash';

const update = (state, mutations) =>
  Object.assign({}, state, mutations)

let nextID

const selectedProjectReducer = (state = null, action) => {
  switch (action.type) {
    case actionType.SELECT_PROJECT:
      return action.project;
    case actionType.UPDATE_SELECTED_PROJECT_WIDTH:
      return update(state, { width: action.value})
    case actionType.UPDATE_SELECTED_PROJECT_HEIGHT:
      return update(state, { height: action.value})
    case actionType.UPDATE_SELECTED_PROJECT_NAME:
      return update(state, { name: action.value})
    case actionType.UPDATE_SELECTED_PROJECT_PADDING:
      return update(state, { padding: action.value})
    case actionType.UPDATE_SELECTED_PROJECT_MARGIN:
      return update(state, { margin: action.value})
    case actionType.ADD_FIGURE:
      if (!state.layout) {
        state.layout = []
      }
      if (state.layout.length > 0) {
        nextID = maxBy(state.layout, 'id').id + 1;
      } else {
        nextID = 1;
      }
      const newFigure = {
        row: action.row,
        id: nextID
      }  
      return {
        ...state,
        layout: [
          ...state.layout,
          newFigure
        ]
      }
    case actionType.SET_FIGURE_FILE:
      return {
        ...state,
        layout: state.layout.map((fig) => fig.id === action.figureID ? { ...fig, url: action.url} : fig)
      }
    case actionType.SET_FIGURE_LABEL:
      return {
        ...state,
        layout: state.layout.map((fig) => fig.id === action.figureID ? { ...fig, label: action.label} : fig)
      }
    case actionType.REMOVE_FIGURE:
      return {
        ...state,
        layout: state.layout.filter((fig) => fig.id !== action.id )
      }
    default:
      return state
  }
}

export default selectedProjectReducer;
