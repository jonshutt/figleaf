import * as actionType from '../actions/ActionType';

const imageChooserReducer = (state = { id: null }, action) => {
  switch (action.type) {
    case actionType.OPEN_IMAGE_CHOOSER:
      return {
        id: action.id
      };
    case actionType.CLOSE_IMAGE_CHOOSER:
      return {
        id: null
      };
    default:
      return state
  }
}

export default imageChooserReducer;
