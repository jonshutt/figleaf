import * as actionType from '../actions/ActionType';

const projectReducer = (state = [], action) => {
  switch (action.type) {
    case actionType.LOADED_PROJECTS:
      // console.log(action)
      const projects = action.projects
      projects.forEach((a, k) => {
        a.layout = JSON.parse(a.layout)
      })
      return projects
    default:
      return state
  }
}

export default projectReducer;
