import React, { Component } from 'react';
import { connect } from 'react-redux';
import { requestFiles } from './actions';

import Page from './components/Page';
import Build from './components/Build';
import Projects from './components/Projects'
import ProjectSettings from './components/ProjectSettings'
import Message from './components/Message'

import './App.css';

class App extends Component {


  componentDidMount() {
    this.props.requestFiles();
  }

  render() {

    return (
      <div className="App">
        <div className="leftCol">
          <Projects />
          <ProjectSettings />
          <Build />
          <Message />
        </div>
        <div className="rightCol">
          <Page />
        </div>
      </div>
    );
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    requestFiles : () => dispatch(requestFiles()),
  }
}

const mapStateToProps = state => {
  return {}
}


export default connect(mapStateToProps,mapDispatchToProps)(App)
