import React, { Component } from 'react';
import { connect } from 'react-redux'
import { setFigureFile, closeImageChooser } from '../actions';
import './ImageChooser.css'
class ImageChooser extends Component {


  setFile(file) {
    this.props.setFigureFile(this.props.id, file)
    this.props.closeImageChooser()
  }

  render() {

    if (!this.props.id) return null


    var files = this.props.files.map((file, key) => {
      return (<p key={key} onClick={() => { this.setFile(file)}}>{file}</p>)
    })

    return (
      <div className="imageChooser">
        {files}
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    id: state.imageChooser.id,
    files: state.files
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setFigureFile: (figureID, url) => dispatch(setFigureFile(figureID, url)),
    closeImageChooser: () => dispatch(closeImageChooser())
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ImageChooser)
