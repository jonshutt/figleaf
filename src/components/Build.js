import React, { Component } from 'react';
import { connect } from 'react-redux'
import axios from 'axios'
import {Icon} from "react-font-awesome-5";

import { requestBuild } from '../actions'
import './Build.css'

class Build extends Component {
  constructor() {
    super();
    this.state = { message: '' };
  }

  makePDF() {
    this.props.requestBuild(this.props.selectedProject)
  }

  render() {
    if (!this.props.selectedProject) return null

    return (
      <div className="build">
        <a className="buildButton" onClick={ () => this.makePDF() }><Icon.FilePdf  /> Create PDF</a>
        {this.state.message}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    selectedProject: state.selectedProject,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    requestBuild : (project) => dispatch(requestBuild(project))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Build)
