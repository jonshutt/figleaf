import React, { Component } from 'react';
import { connect } from 'react-redux';
import { requestProjects, selectProject, addProject, saveProject } from '../actions';
import { confirmAlert } from 'react-confirm-alert'; // Import
import './confirm-alert.css' // Import css
import {Icon} from "react-font-awesome-5";

import './Projects.css';
class Projects extends Component {


  componentDidMount() {
    this.props.requestProjects();
  }

  selectProject(area) {
    if (this.props.selectedProject) {
      confirmAlert({
        title: 'Save Current Project?',
        buttons: [       
          {
            label: 'No',
            onClick: () => this.props.selectProject(area)
          },
          {
            label: 'Yes',
            onClick: () => {
              this.props.saveProject(this.props.selectedProject)
              this.props.selectProject(area)
            },
            
          },
          {
            label: 'Cancel'
          }
        ]
      })
    } else {
      this.props.selectProject(area)
    }
    
    
  }

  render() {


    const projects = this.props.projects.map((area) => <p
      key={area.id}
      className={(this.props.selectedProject && (area.id === this.props.selectedProject.id)) ? 'project selected' : 'project' }
      onClick={ () => { this.selectProject(area)} }
      ><Icon.File  />  {area.name}</p>)

    return (
      <div className="projects">
        <a className="addNew" onClick={()=> { this.props.addProject() }}><Icon.Plus  /> Add New Project</a>
        {projects}
      </div>
    );
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    requestProjects : () => dispatch(requestProjects()),
    selectProject : (area) => dispatch(selectProject(area)),
    addProject : () => dispatch(addProject()),
    saveProject : (project) => dispatch(saveProject(project)),
  }
}

const mapStateToProps = state => {
  return {
    projects: state.projects,
    selectedProject: state.selectedProject,
  }
}


export default connect(mapStateToProps,mapDispatchToProps)(Projects)
