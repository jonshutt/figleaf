import React, { Component } from 'react';
import { connect } from 'react-redux';
import { saveProject,
         updateSelectedProjectHeight,
         updateSelectedProjectWidth,
         updateSelectedProjectName,
         updateSelectedProjectPadding,
         updateSelectedProjectMargin
         } from '../actions';
import {Icon} from "react-font-awesome-5";
import { confirmAlert } from 'react-confirm-alert'; // Import
import './confirm-alert.css' // Import css

import './ProjectSettings.css';
class ProjectSettings extends Component {

  handleWidthChange(e) {
    this.props.updateSelectedProjectWidth(e.target.value)
  }
  handleHeightChange(e) {
    this.props.updateSelectedProjectHeight(e.target.value)
  }
  handleNameChange(e) {
    this.props.updateSelectedProjectName(e.target.value)
  }
  handleMarginChange(e) {
    this.props.updateSelectedProjectMargin(e.target.value)
  }
  handlePaddingChange(e) {
    this.props.updateSelectedProjectPadding(e.target.value)
  }
  saveProject(project) {
    confirmAlert({
      title: 'Confirm Save',
      buttons: [       
        {
          label: 'No'
        },
        {
          label: 'Yes',
          onClick: () => this.props.saveProject(project)
        }
      ]
    })
  }

  render() {
    if (!this.props.selectedProject) return null
    const { width, height, name, padding, margin } = this.props.selectedProject
    return (
      <div className="projectSettings">
        <h3>Settings </h3>
        
        <label className="label">          
          <span>Name</span>
          <input
            value={name}
            onChange={(e) => this.handleNameChange(e)} />
        </label>

        <label className="label">          
          <span>Page Width (cm)</span>
          <input
            value={width}
            onChange={(e) => this.handleWidthChange(e)} />
        </label>
        
        <label className="label">          
          <span>Page Height (cm)</span>
          <input
            value={height}
            onChange={(e) => this.handleHeightChange(e)} />
        </label>
        
        <label className="label">          
          <span>Margin (cm)</span>
          <input
            value={margin}
            onChange={(e) => this.handleMarginChange(e)} />
        </label>
        
        <label className="label">          
          <span>Spacing (cm)</span>
          <input
            value={padding}
            onChange={(e) => this.handlePaddingChange(e)} />
        </label>

        <p>
          <a className="btnSave" onClick={()=> { this.saveProject(this.props.selectedProject) }}><Icon.Save  /> Save</a>
        </p>
      </div>
    );
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    updateSelectedProjectWidth : (value) => dispatch(updateSelectedProjectWidth(value)),
    updateSelectedProjectHeight : (value) => dispatch(updateSelectedProjectHeight(value)),
    updateSelectedProjectName : (value) => dispatch(updateSelectedProjectName(value)),
    updateSelectedProjectPadding : (value) => dispatch(updateSelectedProjectPadding(value)),
    updateSelectedProjectMargin : (value) => dispatch(updateSelectedProjectMargin(value)),
    saveProject: (project) => dispatch(saveProject(project)),
    // addProject : () => dispatch(addProject()),
  }
}

const mapStateToProps = state => {
  return {
    selectedProject: state.selectedProject
  }
}


export default connect(mapStateToProps,mapDispatchToProps)(ProjectSettings)
