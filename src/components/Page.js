import React, { Component } from 'react';
import Figures from './Figures';
import ImageChooser from './ImageChooser';
import { connect } from 'react-redux'
import { cmToPx } from '../Utils'


class Page extends Component {


  render() {
    if (!this.props.selectedProject) return null
    const { width, height } = this.props.selectedProject

    var style={
      'background': '#fff',
      'width': cmToPx(width),
      'height': cmToPx(height),
      'position': 'relative'
    }

    return (

      <div style={style}>
        <Figures />
        <ImageChooser />
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    selectedProject: state.selectedProject
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // setMessage: (message) => dispatch(setMessage(message)),
    // setStatusCenter: (message) => dispatch(setStatusCenter(message)),
    // setMap : (lat, lng, zoom) => dispatch(setMap(lat, lng, zoom)),
  }
}


//export default  Questions
export default connect(mapStateToProps, mapDispatchToProps)(Page)
// export default withRouter(connect(mapStateToProps)(Map));
