import React, { Component } from 'react';
import { connect } from 'react-redux'
import { openImageChooser, removeFigure, updateFigLable } from '../actions';
import './Fig.css'
class Fig extends Component {


  chooseImage() {
    this.props.openImageChooser(this.props.id)
  }

  removeFigure() {
    this.props.removeFigure(this.props.id)
  }

  handleLabelChange(e) {
    this.props.updateFigLable(this.props.id, e.target.value)
  }

  render() {
    const { width, height, x, y } = this.props
    return (
      <div className="fig"  style={{'width': width, 'height': height, 'top': y, 'left': x }}>
        <p>{this.props.url}</p>
        {this.props.children}
        <button onClick={() => this.chooseImage()}>Choose Image</button>
        <button onClick={() => this.removeFigure()}>Remove Figure</button>
        <input
            value={this.props.label}
            onChange={(e) => this.handleLabelChange(e)} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    files: state.files
  }
}

const mapDispatchToProps = dispatch => {
  return {
    openImageChooser: (id) => dispatch(openImageChooser(id)),
    removeFigure: (id) => dispatch(removeFigure(id)),
    updateFigLable: (id, label) => dispatch(updateFigLable(id, label))
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Fig)
