import React, { Component } from 'react';
import { groupBy, values, sortBy } from 'lodash'
import { connect } from 'react-redux'
import { cmToPx } from '../Utils'
import Fig from './Fig'
import { addFigure } from '../actions';
import {Icon} from "react-font-awesome-5";

class Figures extends Component {

  addFigure(row) {
    this.props.addFigure(row)
  }

  render() {
    const { width, height, layout, margin = 1, padding = .5  } = this.props.selectedProject

    const pageMargin = cmToPx(margin)
    const figSpacing = cmToPx(padding)

    const pageHeight = cmToPx(height)
    const pageWidth = cmToPx(width)

    const rows = values(groupBy(sortBy(layout, 'row'), 'row'));

    const totalVerticalPadding = figSpacing * (rows.length-1)
    const totalVerticalFigSize = pageHeight - pageMargin - pageMargin - totalVerticalPadding
    const rowHeight = totalVerticalFigSize / rows.length

    const layoutFigures = [];

    rows.forEach((row, rowID) => {
      const totalHorizontalPadding = figSpacing * (row.length-1)
      const totalHorizontalFigSize = pageWidth - pageMargin - pageMargin - totalHorizontalPadding
      const colWidth = totalHorizontalFigSize / row.length

      let figY  = (rowHeight * rowID) + pageMargin + (figSpacing * rowID)


      row.forEach((fig, colID) => {
        let figX  = (colWidth * colID) + pageMargin + (figSpacing * colID)

        let addRight
        let addBelow

        if ((colID+1) === row.length) {
          addRight = <div className="addRight" onClick={() => { this.addFigure(fig.row) }}><Icon.Plus  /></div>
          if ((rowID+1) === rows.length) {
            addBelow = <div  className="addBelow" onClick={() => { this.addFigure(fig.row + 1 ) }}><Icon.Plus  /></div>
          }
        }

        layoutFigures.push(
          <Fig key={layoutFigures.length} url={fig.url} label={fig.label}  height={rowHeight} width={colWidth} x={figX} y={figY} id={fig.id}>
            {addRight}
            {addBelow}
          </Fig>
        )
      })
    })

    if (rows.length === 0) {
      layoutFigures.push(<button onClick={() => { this.addFigure(0) }}>Add A Figure</button>)
    }

    return (
      <div>
        {layoutFigures}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    selectedProject: state.selectedProject
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addFigure: (row) => dispatch(addFigure(row))
  }
}


//export default  Questions
export default connect(mapStateToProps, mapDispatchToProps)(Figures)
// export default withRouter(connect(mapStateToProps)(Map));
