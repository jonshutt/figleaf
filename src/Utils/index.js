
export const cmToPx = (cm) => {
  return (cm / 2.54) * 72
};
