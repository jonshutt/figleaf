import * as actionType from './ActionType';
import axios from 'axios'

// modal
export const openImageChooser = (id) => ({
  type: actionType.OPEN_IMAGE_CHOOSER,
  id
});

export const closeImageChooser = () => ({
  type: actionType.CLOSE_IMAGE_CHOOSER
});

export const addFigure = (row) => ({
  type: actionType.ADD_FIGURE,
  row
});

export const removeFigure = (id) => ({
  type: actionType.REMOVE_FIGURE,
  id
});

export const requestFiles = () => {
  return dispatch => {

    axios.get('/api/files')
      .then((response) => {
        return dispatch(receiveFiles(response.data.files))
      })
      .catch((error) => {
        console.log(error);
      });
  }
}

export const requestBuild = (project) => {
  
  return dispatch => {
    dispatch(setMessage('Building PDF'))
    axios.post('/api/build', { project })
      .then((response) => {
        dispatch(setMessage(response.data.message))
      })
      .catch((error) => {
        console.log(error);
      });
  }
}

const receiveFiles = (json) => {
  return {
    type: actionType.LOAD_FILES,
    files: json
  }
}


export const setMessage = (message) => ({
  type: actionType.SET_MESSAGE,
  message
});


export const setFigureFile = (figureID, url) => ({
  type: actionType.SET_FIGURE_FILE,
  figureID,
  url
});

export const updateFigLable = (figureID, label) => ({
  type: actionType.SET_FIGURE_LABEL,
  figureID,
  label
});



export const requestProjects = () => {
  return dispatch => {

    axios.get('/api/projects')
      .then((response) => {
        //dispatch(setMessage(response.data.message))
        return dispatch(receiveProjects(response.data.projects))
      })
      .catch((error) => {
        console.log(error);
      });
  }
}

const receiveProjects = (json) => {
  return {
    type: actionType.LOADED_PROJECTS,
    projects: json
  }
}

export const selectProject = (project) => {
  return {
    type: actionType.SELECT_PROJECT,
    project
  }
}



export const addProject = () => {
  return dispatch => {

    axios.post('/api/projects/add')
      .then((response) => {
        dispatch(setMessage(response.data.message))
        return dispatch(requestProjects())
      })
      .catch((error) => {
        console.log(error);
      });
  }
}

export const saveProject = (project) => {
  return dispatch => {
    dispatch(setMessage('Saving Project'))
    axios.post('/api/projects/save', {...project, layout:JSON.stringify(project.layout)})
      .then((response) => {
        dispatch(setMessage(response.data.message))
        return dispatch(requestProjects())
      })
      .catch((error) => {
        console.log(error);
      });
  }
}

export const updateSelectedProjectWidth = (value) => {
  return {
    type: actionType.UPDATE_SELECTED_PROJECT_WIDTH,
    value
  }
}

export const updateSelectedProjectHeight = (value) => {
  return {
    type: actionType.UPDATE_SELECTED_PROJECT_HEIGHT,
    value
  }
}

export const updateSelectedProjectName = (value) => {
  return {
    type: actionType.UPDATE_SELECTED_PROJECT_NAME,
    value
  }
}

export const updateSelectedProjectMargin = (value) => {
  return {
    type: actionType.UPDATE_SELECTED_PROJECT_MARGIN,
    value
  }
}

export const updateSelectedProjectPadding = (value) => {
  return {
    type: actionType.UPDATE_SELECTED_PROJECT_PADDING,
    value
  }
}
