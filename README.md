# Express React Starter

A node.js + react.js based interface for combining figures for publications.

## Prerequisites
* NODE.js

## Installing

Clone or download this repo
npm install

## Running The App

npm start
view the app in any browser at http://localhost:3000/


# Usage

Add your figures to the '/files 'folder
In the browser interface add figures and choose settings

Click 'build'
A PDF file combining all your figures will be generated and saved in the '/output' folder.
